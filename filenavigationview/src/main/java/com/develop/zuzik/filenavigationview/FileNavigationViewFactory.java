package com.develop.zuzik.filenavigationview;

import com.develop.zuzik.navigationview.core.interfaces.NavigationView;
import com.develop.zuzik.navigationview.core.interfaces.NavigationViewContainer;

/**
 * User: zuzik
 * Date: 7/25/16
 */
public interface FileNavigationViewFactory {
	NavigationView create(String path, Object token, NavigationViewContainer container, NavigationView parent);
}
