package com.develop.zuzik.filenavigationview;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * User: zuzik
 * Date: 7/24/16
 */
public class PathUtils {

	static List<String> getFilesPaths(String path) {
		List<String> paths = new ArrayList<>();
		File file = new java.io.File(path);
		paths.add(0, file.getAbsolutePath());
		while ((file = file.getParentFile()) != null) {
			paths.add(0, file.getAbsolutePath());
		}
		return paths;
	}
}
