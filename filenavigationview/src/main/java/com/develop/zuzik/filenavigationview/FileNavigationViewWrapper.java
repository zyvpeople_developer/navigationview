package com.develop.zuzik.filenavigationview;

import android.util.Log;

import com.develop.zuzik.navigationview.core.interfaces.NavigationView;
import com.develop.zuzik.navigationview.core.transaction.Transaction;
import com.develop.zuzik.navigationview.core.wrapper.NavigationViewWrapper;

import java.util.ArrayList;
import java.util.List;

/**
 * User: zuzik
 * Date: 7/24/16
 */
public class FileNavigationViewWrapper extends NavigationViewWrapper {

	private final FileNavigationViewFactory childFactory;

	public FileNavigationViewWrapper(NavigationView parent, FileNavigationViewFactory childFactory) {
		super(parent);
		this.childFactory = childFactory;
	}

	public void navigateToFile(String absolutePath) {
		if (fileExists(absolutePath)) {
			navigateToExistedFile(absolutePath);
			return;
		}

		List<String> newFilePaths = getFilePaths(absolutePath);
		List<String> oldFilePaths = getExistedFilePaths();

		if (oldFilePaths.isEmpty()) {
			addChildren(newFilePaths, 0);
			return;
		}

		int indexOfFirstNotEqualFilePath = getIndexOfFirstNotEqualFilePath(oldFilePaths, newFilePaths);

		if (indexOfFirstNotEqualFilePath == -1) {
			if (newFilePaths.size() > oldFilePaths.size()) {
				addChildren(newFilePaths, oldFilePaths.size());
				return;
			} else {
				Log.w(getClass().getSimpleName(), "New files count must be greater than old files count");
				return;
			}
		}

		Transaction transaction = getParent().beginTransaction();

		for (int i = indexOfFirstNotEqualFilePath; i < oldFilePaths.size(); i++) {
			transaction.removeViewWithToken(getParent().getViews().get(i).getToken());
		}
		for (int i = indexOfFirstNotEqualFilePath; i < newFilePaths.size(); i++) {
			String path = newFilePaths.get(i);
			transaction.addViewToPosition(this.childFactory.create(path, path, getParent().getContainer(), getParent()), i);
		}
		transaction
				.goToViewWithToken(newFilePaths.get(newFilePaths.size() - 1))
				.commit();
	}

	public void navigateBack() {
		NavigationView currentView = getParent().findCurrentView();
		if (currentView == null) {
			Log.w(getClass().getSimpleName(), "Attempt to navigate back when current view does not exist");
			return;
		}
		int indexOfCurrentView = getParent().getViews().indexOf(currentView);
		if (indexOfCurrentView == 0) {
			Log.w(getClass().getSimpleName(), "Attempt to navigate back when current view at zero position");
			return;
		}
		NavigationView viewToGo = getParent().getViews().get(indexOfCurrentView - 1);
		getParent()
				.beginTransaction()
				.goToViewWithToken(viewToGo.getToken())
				.commit();
	}

	public void removeFile(String absolutePath) {
		NavigationView viewToRemove = getParent().findViewWithToken(absolutePath);
		if (viewToRemove == null) {
			Log.w(getClass().getSimpleName(), "Attempt to remove not existed view");
			return;
		}

		int positionToStartDeleting = getParent().getViews().indexOf(viewToRemove);
		if (positionToStartDeleting == 0) {
			Log.w(getClass().getSimpleName(), "Attempt to remove view at zero position");
			return;
		}

		int currentViewPosition = getParent().getViews().indexOf(getParent().findCurrentView());

		Transaction transaction = getParent().beginTransaction();

		for (int i = positionToStartDeleting; i < getParent().getViews().size(); i++) {
			transaction.removeViewWithToken(getParent().getViews().get(i).getToken());
		}

		transaction
				.goToViewWithToken(
						currentViewPosition < positionToStartDeleting
								? getParent().getViews().get(currentViewPosition).getToken()
								: getParent().getViews().get(positionToStartDeleting - 1).getToken())
				.commit();
	}

	private boolean fileExists(String absolutePath) {
		return getFileIndex(absolutePath) != -1;
	}

	private int getFileIndex(String absolutePath) {
		for (int i = 0; i < getParent().getViews().size(); i++) {
			if (getParent().getViews().get(i).getToken().equals(absolutePath)) {
				return i;
			}
		}
		return -1;
	}

	private void navigateToExistedFile(String absolutePath) {
		getParent()
				.beginTransaction()
				.goToViewWithToken(
						getParent()
								.getViews()
								.get(getFileIndex(absolutePath))
								.getToken())
				.commit();
	}

	private List<String> getFilePaths(String absolutePath) {
		return PathUtils.getFilesPaths(absolutePath);
	}

	private List<String> getExistedFilePaths() {
		List<String> paths = new ArrayList<>();
		for (NavigationView view : getParent().getViews()) {
			if (view.getToken() instanceof String) {
				paths.add(view.getToken().toString());
			} else {
				Log.w(getClass().getSimpleName(), String.format("Token '%s' is not String", view.getToken()));
			}
		}
		return paths;
	}

	private int getIndexOfFirstNotEqualFilePath(List<String> oldDirectories, List<String> newDirectories) {
		for (int i = 0; i < Math.min(oldDirectories.size(), newDirectories.size()); i++) {
			if (!oldDirectories.get(i).equals(newDirectories.get(i))) {
				return i;
			}
		}
		return -1;
	}

	private void addChildren(List<String> allPaths, int fromPosition) {
		Transaction transaction = getParent().beginTransaction();
		for (int i = fromPosition; i < allPaths.size(); i++) {
			String path = allPaths.get(i);
			transaction.addViewToPosition(this.childFactory.create(path, path, getParent().getContainer(), getParent()), i);
		}
		transaction
				.goToViewWithToken(allPaths.get(allPaths.size() - 1))
				.commit();
	}
}
