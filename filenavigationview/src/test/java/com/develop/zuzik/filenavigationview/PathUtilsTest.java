package com.develop.zuzik.filenavigationview;

import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;

import static org.junit.Assert.assertEquals;

/**
 * User: zuzik
 * Date: 7/24/16
 */
public class PathUtilsTest {

	@Test
	public void testFilePaths() throws Exception {
		assertEquals(
				Collections.singletonList("/"),
				PathUtils.getFilesPaths("/"));

		assertEquals(
				Arrays.asList("/", "/A"),
				PathUtils.getFilesPaths("/A"));

		assertEquals(
				Arrays.asList("/", "/A", "/A/B"),
				PathUtils.getFilesPaths("/A/B"));

		assertEquals(
				Arrays.asList("/", "/A", "/A/B", "/A/B/C"),
				PathUtils.getFilesPaths("/A/B/C"));
	}
}