package com.develop.zuzik.navigationview.core.transaction;

import com.develop.zuzik.navigationview.core.interfaces.NavigationView;
import com.develop.zuzik.navigationview.core.transaction.action.Action;
import com.develop.zuzik.navigationview.core.transaction.action.AddViewToPositionAction;
import com.develop.zuzik.navigationview.core.transaction.action.GoToViewWithTokenAction;
import com.develop.zuzik.navigationview.core.transaction.action.RemoveViewWithTokenAction;

import java.util.ArrayList;
import java.util.List;

/**
 * User: zuzik
 * Date: 7/23/16
 */
public class Transaction {

	public static Transaction empty() {
		return new Transaction(new TransactionListener() {
			@Override
			public void onCommit(List<Action> actions) {
			}
		});
	}

	public List<Action> actions = new ArrayList<>();
	private TransactionListener listener;

	public Transaction(TransactionListener listener) {
		this.listener = listener;
	}

	public Transaction goToViewWithToken(Object token) {
		return addAction(new GoToViewWithTokenAction(token));
	}

	public Transaction addViewToPosition(NavigationView view, int position) {
		return addAction(new AddViewToPositionAction(view, position));
	}

	public Transaction removeViewWithToken(Object token) {
		return addAction(new RemoveViewWithTokenAction(token));
	}

	private Transaction addAction(Action action) {
		this.actions.add(action);
		return this;
	}

	public void commit() {
		this.listener.onCommit(this.actions);
		this.listener = null;
	}
}
