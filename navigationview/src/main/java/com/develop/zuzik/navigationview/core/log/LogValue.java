package com.develop.zuzik.navigationview.core.log;

/**
 * User: zuzik
 * Date: 7/23/16
 */
public enum LogValue {
	TOKEN,
	IS_STARTED,
	CURRENT_VIEW_TOKEN
}
