package com.develop.zuzik.navigationview.list;

import android.content.Context;
import android.view.View;
import android.widget.FrameLayout;

import com.develop.zuzik.navigationview.R;
import com.develop.zuzik.navigationview.core.interfaces.NavigationView;
import com.develop.zuzik.navigationview.core.interfaces.NavigationViewContainer;
import com.develop.zuzik.navigationview.core.interfaces.NavigationViewHistoryStrategy;
import com.develop.zuzik.navigationview.core.model.NavigationModel;
import com.develop.zuzik.navigationview.core.model.NavigationModelListener;
import com.develop.zuzik.navigationview.core.model.NavigationModelState;
import com.develop.zuzik.navigationview.core.transaction.Transaction;
import com.develop.zuzik.navigationview.core.view.CompositeNavigationView;

import java.util.List;

/**
 * User: zuzik
 * Date: 7/26/16
 */
public class ListNavigationView extends CompositeNavigationView {

	private final NavigationModel model;
	private final FrameLayout frameLayout;

	public ListNavigationView(
			Context context,
			Object token,
			NavigationViewContainer container,
			NavigationView parent,
			NavigationViewHistoryStrategy historyStrategy) {
		super(context, token, container, parent);
		inflate(context, R.layout.view_list_navigation, this);
		this.frameLayout = (FrameLayout) findViewById(R.id.frameLayout);
		this.model = new NavigationModel(historyStrategy);
		this.model.setListener(this.listener);
	}

	@Override
	protected void doOnStart() {
		super.doOnStart();
		this.model.start();
	}

	@Override
	protected void doOnStop() {
		super.doOnStop();
		this.model.stop();
	}

	@Override
	public List<NavigationView> getViews() {
		return this.model.getState().views;
	}

	@Override
	public NavigationView findCurrentView() {
		return this.model.getState().currentViewOrNull;
	}

	@Override
	public NavigationView findViewWithToken(Object token) {
		for (NavigationView view : this.model.getState().views) {
			if (view.getToken().equals(token)) {
				return view;
			}
		}
		return null;
	}

	@Override
	public boolean viewWithTokenExists(Object token) {
		return findViewWithToken(token) != null;
	}

	@Override
	public Transaction beginTransaction() {
		return this.model.beginTransaction();
	}

	private final NavigationModelListener listener = new NavigationModelListener() {
		@Override
		public void onUpdate(NavigationModelState state) {
			View visibleViewOrNull = null;
			for (int i = 0; i < frameLayout.getChildCount(); i++) {
				View child = frameLayout.getChildAt(i);
				if (child.getVisibility() == VISIBLE) {
					visibleViewOrNull = child;
					break;
				}
			}

			frameLayout.removeAllViews();

			for (NavigationView view : state.views) {
				View viewToAdd = (View) view;
				frameLayout.addView(viewToAdd);
				viewToAdd.setVisibility(view == state.currentViewOrNull ? VISIBLE : INVISIBLE);
			}

			if (state.currentViewOrNull != null && visibleViewOrNull != state.currentViewOrNull) {
				model.onSwitchedToPosition(model.getState().views.indexOf(state.currentViewOrNull));
			}
		}
	};
}

