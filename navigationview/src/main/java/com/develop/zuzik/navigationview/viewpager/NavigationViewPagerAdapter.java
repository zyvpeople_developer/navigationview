package com.develop.zuzik.navigationview.viewpager;

import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;

import com.develop.zuzik.navigationview.core.interfaces.NavigationView;
import com.develop.zuzik.navigationview.core.interfaces.NavigationViewTitleFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * User: zuzik
 * Date: 7/21/16
 */
class NavigationViewPagerAdapter extends PagerAdapter {

	private final NavigationViewTitleFactory titleFactory;
	public final List<NavigationView> views = new ArrayList<>();

	public NavigationViewPagerAdapter(NavigationViewTitleFactory titleFactory) {
		super();
		this.titleFactory = titleFactory;
	}

	@Override
	public int getCount() {
		return this.views.size();
	}

	@Override
	public boolean isViewFromObject(View view, Object object) {
		return view == object;
	}

	@Override
	public CharSequence getPageTitle(int position) {
		return this.titleFactory.create(this.views.get(position).getToken());
	}

	@Override
	public Object instantiateItem(ViewGroup container, int position) {
		NavigationView view = this.views.get(position);
		container.addView((View) view, 0);
		return view;
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		container.removeView((View) object);
	}

	@Override
	public int getItemPosition(Object object) {
		return POSITION_NONE;
	}
}
