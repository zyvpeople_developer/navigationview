package com.develop.zuzik.navigationview.core.model;

import com.develop.zuzik.navigationview.core.interfaces.NavigationView;

import java.util.ArrayList;
import java.util.List;

/**
 * User: zuzik
 * Date: 7/22/16
 */
public class NavigationModelState {

	public List<NavigationView> views = new ArrayList<>();
	public NavigationView currentViewOrNull;
	public boolean started;

}
