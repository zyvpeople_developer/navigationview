package com.develop.zuzik.navigationview.core.interfaces;

/**
 * User: zuzik
 * Date: 7/22/16
 */
public interface NavigationViewListener {
	void onStart(NavigationView view);

	void onStop(NavigationView view);
}
