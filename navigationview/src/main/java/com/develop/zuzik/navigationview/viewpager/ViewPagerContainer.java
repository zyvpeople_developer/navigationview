package com.develop.zuzik.navigationview.viewpager;

import android.support.v4.view.ViewPager;

/**
 * User: zuzik
 * Date: 7/23/16
 */
public interface ViewPagerContainer {
	ViewPager getViewPager();
}
