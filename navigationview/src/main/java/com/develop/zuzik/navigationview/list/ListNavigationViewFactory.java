package com.develop.zuzik.navigationview.list;

import android.content.Context;
import android.content.ContextWrapper;

import com.develop.zuzik.navigationview.core.interfaces.NavigationView;
import com.develop.zuzik.navigationview.core.interfaces.NavigationViewContainer;
import com.develop.zuzik.navigationview.core.interfaces.NavigationViewFactory;
import com.develop.zuzik.navigationview.core.interfaces.NavigationViewHistoryStrategy;

/**
 * User: zuzik
 * Date: 7/26/16
 */
public class ListNavigationViewFactory implements NavigationViewFactory {

	private final Context context;
	private final NavigationViewHistoryStrategy historyStrategy;

	public ListNavigationViewFactory(Context context, NavigationViewHistoryStrategy historyStrategy) {
		this.context = new ContextWrapper(context).getApplicationContext();
		this.historyStrategy = historyStrategy;
	}

	@Override
	public NavigationView create(Object token, NavigationViewContainer container, NavigationView parent) {
		return new ListNavigationView(this.context, token, container, parent, this.historyStrategy);
	}
}
