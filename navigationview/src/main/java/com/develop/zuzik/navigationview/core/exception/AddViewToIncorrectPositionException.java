package com.develop.zuzik.navigationview.core.exception;

/**
 * User: zuzik
 * Date: 7/22/16
 */
public class AddViewToIncorrectPositionException extends RuntimeException {

	public AddViewToIncorrectPositionException(int viewsCount, int positionToAddView) {
		super(String.format("Attempt to add view in position %s while views count is %s", viewsCount, positionToAddView));
	}
}
