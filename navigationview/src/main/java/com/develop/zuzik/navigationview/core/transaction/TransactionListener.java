package com.develop.zuzik.navigationview.core.transaction;

import com.develop.zuzik.navigationview.core.transaction.action.Action;

import java.util.List;

/**
 * User: zuzik
 * Date: 7/23/16
 */
public interface TransactionListener {

	void onCommit(List<Action> actions);
}
