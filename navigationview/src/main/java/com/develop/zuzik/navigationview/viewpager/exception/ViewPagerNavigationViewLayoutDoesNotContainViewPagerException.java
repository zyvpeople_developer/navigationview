package com.develop.zuzik.navigationview.viewpager.exception;

/**
 * User: zuzik
 * Date: 7/23/16
 */
public class ViewPagerNavigationViewLayoutDoesNotContainViewPagerException extends RuntimeException {

	public ViewPagerNavigationViewLayoutDoesNotContainViewPagerException() {
		super("Layout must contain ViewPager with id R.id.viewPager");
	}
}
