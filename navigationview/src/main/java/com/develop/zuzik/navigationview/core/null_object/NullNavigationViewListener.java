package com.develop.zuzik.navigationview.core.null_object;

import com.develop.zuzik.navigationview.core.interfaces.NavigationView;
import com.develop.zuzik.navigationview.core.interfaces.NavigationViewListener;

/**
 * User: zuzik
 * Date: 7/22/16
 */
public class NullNavigationViewListener implements NavigationViewListener {

	public static final NullNavigationViewListener INSTANCE = new NullNavigationViewListener();

	private NullNavigationViewListener() {
	}

	@Override
	public void onStart(NavigationView view) {

	}

	@Override
	public void onStop(NavigationView view) {

	}
}
