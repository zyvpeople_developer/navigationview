package com.develop.zuzik.navigationview.viewpager;

import android.content.Context;
import android.content.ContextWrapper;

import com.develop.zuzik.navigationview.core.interfaces.NavigationView;
import com.develop.zuzik.navigationview.core.interfaces.NavigationViewContainer;
import com.develop.zuzik.navigationview.core.interfaces.NavigationViewFactory;
import com.develop.zuzik.navigationview.viewpager.component.ComponentBuilder;

/**
 * User: zuzik
 * Date: 7/22/16
 */
public class ViewPagerNavigationViewFactory implements NavigationViewFactory {

	private final Context context;
	private final ComponentBuilder componentBuilder;

	public ViewPagerNavigationViewFactory(Context context, ComponentBuilder componentBuilder) {
		this.context = new ContextWrapper(context).getApplicationContext();
		this.componentBuilder = componentBuilder;
	}

	@Override
	public NavigationView create(Object token, NavigationViewContainer container, NavigationView parent) {
		return new ViewPagerNavigationView(this.context, token, container, parent, this.componentBuilder);
	}
}
