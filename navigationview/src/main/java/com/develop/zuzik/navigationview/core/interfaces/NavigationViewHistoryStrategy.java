package com.develop.zuzik.navigationview.core.interfaces;

import com.develop.zuzik.navigationview.core.model.NavigationModelState;

/**
 * User: zuzik
 * Date: 7/22/16
 */
public interface NavigationViewHistoryStrategy {
	void modifyState(NavigationModelState outState);
}
