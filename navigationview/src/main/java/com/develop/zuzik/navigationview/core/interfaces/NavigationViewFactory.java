package com.develop.zuzik.navigationview.core.interfaces;

/**
 * User: zuzik
 * Date: 7/22/16
 */
public interface NavigationViewFactory {
	NavigationView create(Object token, NavigationViewContainer container, NavigationView parent);
}
