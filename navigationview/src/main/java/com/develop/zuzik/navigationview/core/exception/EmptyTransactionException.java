package com.develop.zuzik.navigationview.core.exception;

/**
 * User: zuzik
 * Date: 7/23/16
 */
public class EmptyTransactionException extends RuntimeException {

	public EmptyTransactionException() {
		super("Transaction can't be empty");
	}
}
