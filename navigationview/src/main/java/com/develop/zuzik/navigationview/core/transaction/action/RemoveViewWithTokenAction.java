package com.develop.zuzik.navigationview.core.transaction.action;

/**
 * User: zuzik
 * Date: 7/23/16
 */
public class RemoveViewWithTokenAction implements Action {

	public final Object token;

	public RemoveViewWithTokenAction(Object token) {
		this.token = token;
	}
}
