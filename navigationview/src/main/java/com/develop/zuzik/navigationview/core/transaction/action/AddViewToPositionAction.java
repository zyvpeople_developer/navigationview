package com.develop.zuzik.navigationview.core.transaction.action;

import com.develop.zuzik.navigationview.core.interfaces.NavigationView;

/**
 * User: zuzik
 * Date: 7/23/16
 */
public class AddViewToPositionAction implements Action {

	public final NavigationView view;
	public final int position;

	public AddViewToPositionAction(NavigationView view, int position) {
		this.view = view;
		this.position = position;
	}
}
