package com.develop.zuzik.navigationview.core.view;

import android.content.Context;

import com.develop.zuzik.navigationview.core.interfaces.NavigationView;
import com.develop.zuzik.navigationview.core.interfaces.NavigationViewContainer;
import com.develop.zuzik.navigationview.core.transaction.Transaction;

import java.util.ArrayList;
import java.util.List;

/**
 * User: zuzik
 * Date: 7/22/16
 */
public abstract class LeafNavigationView extends CompositeNavigationView {

	public LeafNavigationView(Context context, Object token, NavigationViewContainer container, NavigationView parent) {
		super(context, token, container, parent);
	}

	@Override
	public final List<NavigationView> getViews() {
		return new ArrayList<>();
	}

	@Override
	public final NavigationView findCurrentView() {
		return null;
	}

	@Override
	public final NavigationView findViewWithToken(Object token) {
		return null;
	}

	@Override
	public final boolean viewWithTokenExists(Object token) {
		return false;
	}

	@Override
	public final Transaction beginTransaction() {
		return Transaction.empty();
	}
}
