package com.develop.zuzik.navigationview.viewpager;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.view.MotionEvent;
import android.view.View;

import com.develop.zuzik.navigationview.R;
import com.develop.zuzik.navigationview.core.interfaces.NavigationView;
import com.develop.zuzik.navigationview.core.interfaces.NavigationViewContainer;
import com.develop.zuzik.navigationview.core.model.NavigationModel;
import com.develop.zuzik.navigationview.core.model.NavigationModelListener;
import com.develop.zuzik.navigationview.core.model.NavigationModelState;
import com.develop.zuzik.navigationview.core.transaction.Transaction;
import com.develop.zuzik.navigationview.core.view.CompositeNavigationView;
import com.develop.zuzik.navigationview.viewpager.component.Component;
import com.develop.zuzik.navigationview.viewpager.component.ComponentBuilder;
import com.develop.zuzik.navigationview.viewpager.exception.ViewPagerNavigationViewLayoutDoesNotContainViewPagerException;

import java.util.List;

/**
 * User: zuzik
 * Date: 7/22/16
 */
class ViewPagerNavigationView extends CompositeNavigationView implements ViewPagerContainer {

	private final Component component;
	private final NavigationModel model;
	private final ViewPager viewPager;
	private final NavigationViewPagerAdapter adapter;

	public ViewPagerNavigationView(
			Context context,
			Object token,
			NavigationViewContainer container,
			NavigationView parent,
			ComponentBuilder componentBuilder) {
		super(context, token, container, parent);
		this.component = componentBuilder.build();
		int layoutResId = this.component.layoutResId != 0
				? this.component.layoutResId
				: R.layout.view_pager_navigation;
		inflate(context, layoutResId, this);
		this.viewPager = getViewPagerOrThrowException();
		this.model = new NavigationModel(this.component.historyStrategy);
		this.model.setListener(this.listener);
		this.adapter = new NavigationViewPagerAdapter(this.component.titleFactory);
		this.viewPager.addOnPageChangeListener(this.onPageChangeListener);
		this.viewPager.setAdapter(this.adapter);
		this.viewPager.setOnTouchListener(this.onTouchListenerToAllowSwipe);
		this.viewPager.setPageTransformer(true, this.component.pageTransformer);
	}

	private ViewPager getViewPagerOrThrowException() {
		View possibleViewPager = findViewById(R.id.viewPager);
		if (possibleViewPager != null
				&& possibleViewPager instanceof ViewPager) {
			return (ViewPager) possibleViewPager;
		} else {
			throw new ViewPagerNavigationViewLayoutDoesNotContainViewPagerException();
		}
	}

	@Override
	protected void doOnStart() {
		super.doOnStart();
		this.model.start();
	}

	@Override
	protected void doOnStop() {
		super.doOnStop();
		this.model.stop();
	}

	@Override
	public List<NavigationView> getViews() {
		return this.model.getState().views;
	}

	@Override
	public NavigationView findCurrentView() {
		return this.model.getState().currentViewOrNull;
	}

	@Override
	public NavigationView findViewWithToken(Object token) {
		for (NavigationView view : this.model.getState().views) {
			if (view.getToken().equals(token)) {
				return view;
			}
		}
		return null;
	}

	@Override
	public boolean viewWithTokenExists(Object token) {
		return findViewWithToken(token) != null;
	}

	@Override
	public Transaction beginTransaction() {
		return this.model.beginTransaction();
	}

	//region ViewPagerContainer

	@Override
	public ViewPager getViewPager() {
		return this.viewPager;
	}

	//endregion

	private final NavigationModelListener listener = new NavigationModelListener() {
		@Override
		public void onUpdate(NavigationModelState state) {
			adapter.views.clear();
			adapter.views.addAll(state.views);
			adapter.notifyDataSetChanged();
			if (state.currentViewOrNull == null) {
				viewPager.setVisibility(INVISIBLE);
			} else {
				viewPager.setVisibility(VISIBLE);
				viewPager.setCurrentItem(state.views.indexOf(state.currentViewOrNull), isStarted() && component.allowPageAnimation);
			}
		}
	};

	private final ViewPager.SimpleOnPageChangeListener onPageChangeListener = new ViewPager.SimpleOnPageChangeListener() {
		@Override
		public void onPageSelected(int position) {
			model.onSwitchedToPosition(position);
		}
	};

	private final OnTouchListener onTouchListenerToAllowSwipe = new OnTouchListener() {
		@Override
		public boolean onTouch(View view, MotionEvent motionEvent) {
			return !component.allowSwipe;
		}
	};
}
