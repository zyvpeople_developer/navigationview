package com.develop.zuzik.navigationview.core.exception;

/**
 * User: zuzik
 * Date: 7/22/16
 */
public class UserSwitchedToIncorrectPositionException extends RuntimeException {

	public UserSwitchedToIncorrectPositionException(int viewsCount, int positionToAddView) {
		super(String.format("Attempt switch to position %s while views count is %s", viewsCount, positionToAddView));
	}
}
