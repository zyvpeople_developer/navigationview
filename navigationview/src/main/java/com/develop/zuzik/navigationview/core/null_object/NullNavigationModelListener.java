package com.develop.zuzik.navigationview.core.null_object;

import com.develop.zuzik.navigationview.core.model.NavigationModelListener;
import com.develop.zuzik.navigationview.core.model.NavigationModelState;

/**
 * User: zuzik
 * Date: 7/22/16
 */
public class NullNavigationModelListener implements NavigationModelListener {

	public static final NullNavigationModelListener INSTANCE = new NullNavigationModelListener();

	private NullNavigationModelListener() {
	}

	@Override
	public void onUpdate(NavigationModelState state) {
	}
}
