package com.develop.zuzik.navigationview.core.interfaces;

import com.develop.zuzik.navigationview.core.transaction.Transaction;

import java.util.List;

/**
 * User: zuzik
 * Date: 7/22/16
 */
public interface NavigationView {

	Object getToken();

	NavigationViewContainer getContainer();

	NavigationView findParent();

	void setListener(NavigationViewListener listener);

	boolean isStarted();

	void onStart();

	void onStop();

	List<NavigationView> getViews();

	NavigationView findCurrentView();

	NavigationView findViewWithToken(Object token);

	boolean viewWithTokenExists(Object token);

	Transaction beginTransaction();

}
