package com.develop.zuzik.navigationview.core.model;

import android.util.Log;

import com.develop.zuzik.navigationview.core.exception.AddViewToIncorrectPositionException;
import com.develop.zuzik.navigationview.core.exception.EmptyTransactionException;
import com.develop.zuzik.navigationview.core.exception.LastActionIsNotGoToViewActionException;
import com.develop.zuzik.navigationview.core.exception.MoreThanOneGoToViewActionsException;
import com.develop.zuzik.navigationview.core.exception.UserSwitchedToIncorrectPositionException;
import com.develop.zuzik.navigationview.core.interfaces.NavigationView;
import com.develop.zuzik.navigationview.core.interfaces.NavigationViewHistoryStrategy;
import com.develop.zuzik.navigationview.core.null_object.NullNavigationModelListener;
import com.develop.zuzik.navigationview.core.transaction.Transaction;
import com.develop.zuzik.navigationview.core.transaction.TransactionListener;
import com.develop.zuzik.navigationview.core.transaction.action.Action;
import com.develop.zuzik.navigationview.core.transaction.action.AddViewToPositionAction;
import com.develop.zuzik.navigationview.core.transaction.action.GoToViewWithTokenAction;
import com.develop.zuzik.navigationview.core.transaction.action.RemoveViewWithTokenAction;

import java.util.List;

/**
 * User: zuzik
 * Date: 7/22/16
 */
public class NavigationModel {

	private final NavigationModelState state = new NavigationModelState();
	private NavigationModelListener listener = NullNavigationModelListener.INSTANCE;
	private final NavigationViewHistoryStrategy historyStrategy;

	public NavigationModel(NavigationViewHistoryStrategy historyStrategy) {
		this.historyStrategy = historyStrategy;
	}

	//region Navigation.Model

	public NavigationModelState getState() {
		return this.state;
	}

	public void setListener(NavigationModelListener listener) {
		this.listener = listener != null ? listener : NullNavigationModelListener.INSTANCE;
	}

	public Transaction beginTransaction() {
		return new Transaction(new TransactionListener() {
			@Override
			public void onCommit(List<Action> actions) {
				verifyTransactionActions(actions);
				for (Action action : actions) {
					if (action instanceof AddViewToPositionAction) {
						addViewToPosition((AddViewToPositionAction) action);
					} else if (action instanceof RemoveViewWithTokenAction) {
						removeViewWithToken((RemoveViewWithTokenAction) action);
					} else if (action instanceof GoToViewWithTokenAction) {
						goToViewWithToken((GoToViewWithTokenAction) action);
					} else {
						logW("Unknown action '%s'", action);
					}
				}
				notifyOnUpdate();
			}
		});
	}

	private void addViewToPosition(AddViewToPositionAction action) {
		NavigationView view = action.view;
		int position = action.position;

		verifyPositionToAddView(position);
		if (viewWithTokenExists(view.getToken())) {
			logW("Attempt to add already existed view '%s' with token '%s'", view, view.getToken());
			return;
		}

		getState().views.add(position, view);
	}

	private void removeViewWithToken(RemoveViewWithTokenAction action) {
		Object token = action.token;

		if (!viewWithTokenExists(token)) {
			logW("Attempt to remove not existed view with token '%s'", token);
			return;
		}

		NavigationView currentView = getState().currentViewOrNull;
		if (currentView != null) {
			if (currentView.getToken().equals(token)) {
				stopView(currentView);
				getState().currentViewOrNull = null;
			}
		}
		getState().views.remove(findViewWithTokenOrNull(token));
	}

	private void goToViewWithToken(GoToViewWithTokenAction action) {
		Object token = action.token;
		NavigationView currentView = getState().currentViewOrNull;

		boolean isCurrentView = currentView != null && currentView.getToken().equals(token);
		if (isCurrentView) {
			logW("Attempt to go to current view '%s' with token '%s'", currentView, currentView.getToken());
			return;
		}
		if (!viewWithTokenExists(token)) {
			logW("Attempt to go to not existed view with token '%s'", token);
			return;
		}

		if (currentView != null) {
			stopView(currentView);
		}

		getState().currentViewOrNull = findViewWithTokenOrNull(token);

		this.historyStrategy.modifyState(getState());
	}

	public void start() {
		getState().started = true;
		startView(getState().currentViewOrNull);
	}

	public void stop() {
		stopView(getState().currentViewOrNull);
		getState().started = false;
	}

	public void onSwitchedToPosition(int position) {
		verifyUserSwitchedPosition(position);

		NavigationView switchedView = getState().views.get(position);
		NavigationView currentView = getState().currentViewOrNull;

		boolean notInCurrentView = currentView == null || switchedView != currentView;
		if (notInCurrentView) {
			if (getState().currentViewOrNull != null) {
				stopView(getState().currentViewOrNull);
			}
			getState().currentViewOrNull = switchedView;
			this.historyStrategy.modifyState(getState());
			startView(switchedView);

			notifyOnUpdate();
		} else {
			startView(getState().currentViewOrNull);
		}
	}

	//endregion

	private void startView(NavigationView viewOrNull) {
		if (viewOrNull == null) {
			logW("Attempt to start null view");
		} else if (!getState().started) {
			logW("Attempt to start view '%s' with token '%s' when model is in stopped state", viewOrNull, viewOrNull.getToken());
		} else if (viewOrNull.isStarted()) {
			logW("Attempt to start already started view '%s' with token '%s'", viewOrNull, viewOrNull.getToken());
		} else {
			viewOrNull.onStart();
		}
	}

	private void stopView(NavigationView viewOrNull) {
		if (viewOrNull == null) {
			logW("Attempt to stop null view");
		} else if (!getState().started) {
			logW("Attempt to stop view '%s' with token '%s' when model is in stopped state", viewOrNull, viewOrNull.getToken());
		} else if (!viewOrNull.isStarted()) {
			logW("Attempt to stop already stopped view '%s' with token '%s'", viewOrNull, viewOrNull.getToken());
		} else {
			viewOrNull.onStop();
		}
	}

	private NavigationView findViewWithTokenOrNull(Object token) {
		for (NavigationView view : this.state.views) {
			if (view.getToken().equals(token)) {
				return view;
			}
		}
		return null;
	}

	private boolean viewWithTokenExists(Object token) {
		return findViewWithTokenOrNull(token) != null;
	}

	private void verifyPositionToAddView(int position) {
		boolean positionInCorrectRange = 0 <= position && position <= getState().views.size();
		if (!positionInCorrectRange) {
			throw new AddViewToIncorrectPositionException(getState().views.size(), position);
		}
	}

	private void verifyUserSwitchedPosition(int position) {
		boolean positionInCorrectRange = 0 <= position && position < getState().views.size();
		if (!positionInCorrectRange) {
			throw new UserSwitchedToIncorrectPositionException(getState().views.size(), position);
		}
	}

	private void verifyTransactionActions(List<Action> actions) {
		if (actions.isEmpty()) {
			throw new EmptyTransactionException();
		}
		if (containMoreThanOneGoToViewActions(actions)) {
			throw new MoreThanOneGoToViewActionsException();
		}
		if (!lastActionIsGoToViewAction(actions)) {
			throw new LastActionIsNotGoToViewActionException();
		}
	}

	private boolean containMoreThanOneGoToViewActions(List<Action> actions) {
		int goToViewActionsCount = 0;
		for (Action action : actions) {
			if (action instanceof GoToViewWithTokenAction) {
				goToViewActionsCount++;
			}
		}
		return goToViewActionsCount > 1;
	}

	private boolean lastActionIsGoToViewAction(List<Action> actions) {
		return !actions.isEmpty() && actions.get(actions.size() - 1) instanceof GoToViewWithTokenAction;
	}

	private void notifyOnUpdate() {
		this.listener.onUpdate(getState());
	}

	private void logW(String formattedMessage, Object... args) {
		Log.w(getClass().getSimpleName(), String.format(formattedMessage, args));
	}
}
