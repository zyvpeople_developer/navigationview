package com.develop.zuzik.navigationview.core.log;

import android.util.Log;

import com.develop.zuzik.navigationview.core.interfaces.NavigationView;

import java.util.Arrays;

/**
 * User: zuzik
 * Date: 7/23/16
 */
public class NavigationViewLog {

	public static void logTree(Object tag, NavigationView view, LogValue... logValues) {
		Log.i(
				tag.getClass().getSimpleName(),
				createTreeRepresentation(
						view,
						1,
						shouldLogValue(LogValue.TOKEN, logValues),
						shouldLogValue(LogValue.IS_STARTED, logValues),
						shouldLogValue(LogValue.CURRENT_VIEW_TOKEN, logValues)));
	}

	private static String createTreeRepresentation(
			NavigationView view,
			int level,
			boolean logToken,
			boolean logIsStarted,
			boolean logCurrentViewToken) {
		int spacesCount = 8;
		String spaces = String.format("%" + level * spacesCount + "s", " ");
		String result = "";

		if (logToken) {
			result += String.format("\n%sToken:%s", spaces, view.getToken());
		}

		if (logIsStarted) {
			result += String.format(", IsStarted:%s", view.isStarted());
		}

		if (logCurrentViewToken && view.findCurrentView() != null) {
			result += String.format(", Current view token: %s", view.findCurrentView().getToken());
		}

		for (NavigationView navigationView : view.getViews()) {
			result += createTreeRepresentation(navigationView, level + 1, logToken, logIsStarted, logCurrentViewToken);
		}

		return result;
	}

	private static boolean shouldLogValue(LogValue logValue, LogValue... passedValues) {
		return passedValues.length == 0 || Arrays.asList(passedValues).contains(logValue);
	}
}
