package com.develop.zuzik.navigationview.viewpager.component.null_object;

import android.support.v4.view.ViewPager;
import android.view.View;

/**
 * User: zuzik
 * Date: 7/23/16
 */
public class DefaultPageTransformer implements ViewPager.PageTransformer {

	public static final DefaultPageTransformer INSTANCE = new DefaultPageTransformer();

	private DefaultPageTransformer() {
	}

	@Override
	public void transformPage(View page, float position) {
	}
}
