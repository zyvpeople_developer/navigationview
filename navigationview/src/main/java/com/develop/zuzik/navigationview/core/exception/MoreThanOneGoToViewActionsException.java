package com.develop.zuzik.navigationview.core.exception;

import com.develop.zuzik.navigationview.core.transaction.action.GoToViewWithTokenAction;

/**
 * User: zuzik
 * Date: 7/23/16
 */
public class MoreThanOneGoToViewActionsException extends RuntimeException {

	public MoreThanOneGoToViewActionsException() {
		super(String.format("Transaction must contain only one %s", GoToViewWithTokenAction.class.getSimpleName()));
	}
}
