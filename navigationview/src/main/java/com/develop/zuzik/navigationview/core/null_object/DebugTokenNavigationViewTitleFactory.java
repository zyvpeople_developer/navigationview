package com.develop.zuzik.navigationview.core.null_object;

import com.develop.zuzik.navigationview.core.interfaces.NavigationViewTitleFactory;

/**
 * User: zuzik
 * Date: 7/21/16
 */
public class DebugTokenNavigationViewTitleFactory implements NavigationViewTitleFactory {

	public static final DebugTokenNavigationViewTitleFactory INSTANCE = new DebugTokenNavigationViewTitleFactory();

	private DebugTokenNavigationViewTitleFactory() {
	}

	@Override
	public String create(Object token) {
		return token.toString();
	}
}
