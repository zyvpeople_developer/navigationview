package com.develop.zuzik.navigationview.core.transaction.action;

/**
 * User: zuzik
 * Date: 7/23/16
 */
public class GoToViewWithTokenAction implements Action {

	public final Object token;

	public GoToViewWithTokenAction(Object token) {
		this.token = token;
	}
}
