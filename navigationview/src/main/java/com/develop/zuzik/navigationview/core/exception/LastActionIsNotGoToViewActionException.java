package com.develop.zuzik.navigationview.core.exception;

import com.develop.zuzik.navigationview.core.transaction.action.GoToViewWithTokenAction;

/**
 * User: zuzik
 * Date: 7/23/16
 */
public class LastActionIsNotGoToViewActionException extends RuntimeException {

	public LastActionIsNotGoToViewActionException() {
		super(String.format("Last action must be %s", GoToViewWithTokenAction.class.getSimpleName()));
	}
}
