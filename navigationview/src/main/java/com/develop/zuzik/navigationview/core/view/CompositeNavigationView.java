package com.develop.zuzik.navigationview.core.view;

import android.content.Context;
import android.util.Log;
import android.widget.LinearLayout;

import com.develop.zuzik.navigationview.core.interfaces.NavigationView;
import com.develop.zuzik.navigationview.core.interfaces.NavigationViewContainer;
import com.develop.zuzik.navigationview.core.interfaces.NavigationViewListener;
import com.develop.zuzik.navigationview.core.null_object.NullNavigationViewListener;

/**
 * User: zuzik
 * Date: 7/22/16
 */
public abstract class CompositeNavigationView extends LinearLayout implements NavigationView {

	private final Object token;
	private final NavigationViewContainer container;
	private final NavigationView parent;
	private NavigationViewListener listener = NullNavigationViewListener.INSTANCE;
	private boolean started;

	protected CompositeNavigationView(Context context, Object token, NavigationViewContainer container, NavigationView parent) {
		super(context);
		this.token = token;
		this.container = container;
		this.parent = parent;
	}

	protected void doOnStart() {

	}

	protected void doOnStop() {

	}

	@Override
	public final void onStart() {
		if (this.started) {
			logW("Attempt to start already started view '%s' with token '%s'", this, getToken());
			return;
		}
		this.started = true;
		logD("onStart. Token: " + getToken());
		doOnStart();
		this.listener.onStart(this);
	}

	@Override
	public final void onStop() {
		if (!this.started) {
			logW("Attempt to stop already stopped view '%s' with token '%s'", this, getToken());
			return;
		}
		this.started = false;
		logD("onStop. Token: " + getToken());
		doOnStop();
		this.listener.onStop(this);
	}

	@Override
	public final boolean isStarted() {
		return this.started;
	}

	@Override
	public final void setListener(NavigationViewListener listener) {
		this.listener = listener != null ? listener : NullNavigationViewListener.INSTANCE;
	}

	@Override
	public final NavigationView findParent() {
		return this.parent;
	}

	@Override
	public final NavigationViewContainer getContainer() {
		return this.container;
	}

	@Override
	public final Object getToken() {
		return this.token;
	}

	private void logD(String formattedMessage, Object... args) {
		Log.d(getClass().getSimpleName(), String.format(formattedMessage, args));
	}

	private void logW(String formattedMessage, Object... args) {
		Log.w(getClass().getSimpleName(), String.format(formattedMessage, args));
	}
}
