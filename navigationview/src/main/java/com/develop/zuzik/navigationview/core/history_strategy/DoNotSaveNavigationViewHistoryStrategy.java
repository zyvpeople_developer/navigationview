package com.develop.zuzik.navigationview.core.history_strategy;

import com.develop.zuzik.navigationview.core.interfaces.NavigationViewHistoryStrategy;
import com.develop.zuzik.navigationview.core.model.NavigationModelState;

/**
 * User: zuzik
 * Date: 7/22/16
 */
public class DoNotSaveNavigationViewHistoryStrategy implements NavigationViewHistoryStrategy {

	public static final DoNotSaveNavigationViewHistoryStrategy INSTANCE = new DoNotSaveNavigationViewHistoryStrategy();

	private DoNotSaveNavigationViewHistoryStrategy() {
	}

	@Override
	public void modifyState(NavigationModelState outState) {
		if (outState.currentViewOrNull == null) {
			return;
		}
		int currentViewPosition = outState.views.indexOf(outState.currentViewOrNull);
		if (currentViewPosition == -1) {
			return;
		}
		for (int i = currentViewPosition + 1; i < outState.views.size(); i++) {
			outState.views.remove(i);
			i--;
		}
	}
}
