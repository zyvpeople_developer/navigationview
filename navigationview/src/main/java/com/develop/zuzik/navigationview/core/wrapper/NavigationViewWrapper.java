package com.develop.zuzik.navigationview.core.wrapper;

import com.develop.zuzik.navigationview.core.interfaces.NavigationView;

/**
 * User: zuzik
 * Date: 7/23/16
 */
public abstract class NavigationViewWrapper {

	private final NavigationView parent;

	protected NavigationViewWrapper(NavigationView parent) {
		this.parent = parent;
	}

	protected final NavigationView getParent() {
		return this.parent;
	}
}
