package com.develop.zuzik.navigationview.viewpager.component;

import android.support.annotation.LayoutRes;
import android.support.v4.view.ViewPager;

import com.develop.zuzik.navigationview.core.history_strategy.DoNotSaveNavigationViewHistoryStrategy;
import com.develop.zuzik.navigationview.core.history_strategy.SaveNavigationViewHistoryStrategy;
import com.develop.zuzik.navigationview.core.interfaces.NavigationViewHistoryStrategy;
import com.develop.zuzik.navigationview.core.interfaces.NavigationViewTitleFactory;
import com.develop.zuzik.navigationview.core.null_object.NullNavigationViewTitleFactory;
import com.develop.zuzik.navigationview.viewpager.component.null_object.DefaultPageTransformer;

/**
 * User: zuzik
 * Date: 7/23/16
 */
public class ComponentBuilder {

	private boolean allowPageAnimation = true;
	private ViewPager.PageTransformer pageTransformer = DefaultPageTransformer.INSTANCE;
	private boolean allowSwipe = true;
	private NavigationViewTitleFactory titleFactory = NullNavigationViewTitleFactory.INSTANCE;
	private NavigationViewHistoryStrategy historyStrategy = SaveNavigationViewHistoryStrategy.INSTANCE;
	@LayoutRes
	private int layoutResId;

	public boolean isAllowPageAnimation() {
		return this.allowPageAnimation;
	}

	public ComponentBuilder allowPageAnimation() {
		return setAllowPageAnimation(true);
	}

	public ComponentBuilder denyPageAnimation() {
		return setAllowPageAnimation(false);
	}

	private ComponentBuilder setAllowPageAnimation(boolean allowPageAnimation) {
		this.allowPageAnimation = allowPageAnimation;
		return this;
	}

	public ViewPager.PageTransformer getPageTransformer() {
		return this.pageTransformer;
	}

	public ComponentBuilder setPageTransformer(ViewPager.PageTransformer pageTransformer) {
		this.pageTransformer = pageTransformer;
		return this;
	}

	public boolean isAllowSwipe() {
		return this.allowSwipe;
	}

	public ComponentBuilder allowSwipe() {
		return setAllowSwipe(true);
	}

	public ComponentBuilder denySwipe() {
		return setAllowSwipe(false);
	}

	private ComponentBuilder setAllowSwipe(boolean allowSwipe) {
		this.allowSwipe = allowSwipe;
		return this;
	}

	public NavigationViewTitleFactory getTitleFactory() {
		return this.titleFactory;
	}

	public ComponentBuilder setTitleFactory(NavigationViewTitleFactory titleFactory) {
		this.titleFactory = titleFactory;
		return this;
	}

	public NavigationViewHistoryStrategy getHistoryStrategy() {
		return this.historyStrategy;
	}

	public ComponentBuilder saveViewsHistory() {
		return setHistoryStrategy(SaveNavigationViewHistoryStrategy.INSTANCE);
	}

	public ComponentBuilder doNotSaveViewsHistory() {
		return setHistoryStrategy(DoNotSaveNavigationViewHistoryStrategy.INSTANCE);
	}

	private ComponentBuilder setHistoryStrategy(NavigationViewHistoryStrategy historyStrategy) {
		this.historyStrategy = historyStrategy;
		return this;
	}

	@LayoutRes
	public int getLayoutResId() {
		return this.layoutResId;
	}

	public ComponentBuilder setLayoutResId(@LayoutRes int layoutResId) {
		this.layoutResId = layoutResId;
		return this;
	}

	public Component build() {
		return new Component(
				this.allowPageAnimation,
				this.pageTransformer,
				this.allowSwipe,
				this.titleFactory,
				this.historyStrategy,
				this.layoutResId);
	}
}
