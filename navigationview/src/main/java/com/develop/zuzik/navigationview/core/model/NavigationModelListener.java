package com.develop.zuzik.navigationview.core.model;

/**
 * User: zuzik
 * Date: 7/22/16
 */
public interface NavigationModelListener {
	void onUpdate(NavigationModelState state);
}
