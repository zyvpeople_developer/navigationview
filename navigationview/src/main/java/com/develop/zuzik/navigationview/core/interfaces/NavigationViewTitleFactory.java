package com.develop.zuzik.navigationview.core.interfaces;

/**
 * User: zuzik
 * Date: 7/21/16
 */
public interface NavigationViewTitleFactory {
	String create(Object token);
}
