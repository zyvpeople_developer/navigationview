package com.develop.zuzik.navigationview.viewpager.component;

import android.support.annotation.LayoutRes;
import android.support.v4.view.ViewPager;

import com.develop.zuzik.navigationview.core.interfaces.NavigationViewHistoryStrategy;
import com.develop.zuzik.navigationview.core.interfaces.NavigationViewTitleFactory;

/**
 * User: zuzik
 * Date: 7/23/16
 */
public class Component {
	public final boolean allowPageAnimation;
	public final ViewPager.PageTransformer pageTransformer;
	public final boolean allowSwipe;
	public final NavigationViewTitleFactory titleFactory;
	public final NavigationViewHistoryStrategy historyStrategy;
	@LayoutRes
	public final int layoutResId;

	public Component(
			boolean allowPageAnimation,
			ViewPager.PageTransformer pageTransformer,
			boolean allowSwipe,
			NavigationViewTitleFactory titleFactory,
			NavigationViewHistoryStrategy historyStrategy, int layoutResId) {
		this.allowPageAnimation = allowPageAnimation;
		this.pageTransformer = pageTransformer;
		this.allowSwipe = allowSwipe;
		this.titleFactory = titleFactory;
		this.historyStrategy = historyStrategy;
		this.layoutResId = layoutResId;
	}
}
