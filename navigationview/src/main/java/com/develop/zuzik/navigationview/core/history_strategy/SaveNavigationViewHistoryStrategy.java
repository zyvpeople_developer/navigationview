package com.develop.zuzik.navigationview.core.history_strategy;

import com.develop.zuzik.navigationview.core.interfaces.NavigationViewHistoryStrategy;
import com.develop.zuzik.navigationview.core.model.NavigationModelState;

/**
 * User: zuzik
 * Date: 7/22/16
 */
public class SaveNavigationViewHistoryStrategy implements NavigationViewHistoryStrategy {

	public static final SaveNavigationViewHistoryStrategy INSTANCE = new SaveNavigationViewHistoryStrategy();

	private SaveNavigationViewHistoryStrategy() {
	}

	@Override
	public void modifyState(NavigationModelState outState) {
	}
}
