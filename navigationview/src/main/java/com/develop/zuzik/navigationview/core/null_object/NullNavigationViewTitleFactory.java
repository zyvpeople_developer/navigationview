package com.develop.zuzik.navigationview.core.null_object;

import com.develop.zuzik.navigationview.core.interfaces.NavigationViewTitleFactory;

/**
 * User: zuzik
 * Date: 7/21/16
 */
public class NullNavigationViewTitleFactory implements NavigationViewTitleFactory {

	public static final NullNavigationViewTitleFactory INSTANCE = new NullNavigationViewTitleFactory();

	private NullNavigationViewTitleFactory() {
	}

	@Override
	public String create(Object token) {
		return "";
	}
}
