package com.develop.zuzik.navigationviewexample.view_pager_transformer;

import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.view.View;

/**
 * User: zuzik
 * Date: 7/23/16
 */
public class DepthPageTransformer implements ViewPager.PageTransformer {
	private static final float MIN_SCALE = 0.75f;

	public void transformPage(View view, float position) {
		int pageWidth = view.getWidth();

		if (position < -1) { // [-Infinity,-1)
			// This page is way off-screen to the left.
			setAlpha(view, 0);

		} else if (position <= 0) { // [-1,0]
			// Use the default slide transition when moving to the left page
			setAlpha(view, 1);
			setTranslationX(view, 0);
			setScaleX(view, 1);
			setScaleY(view, 1);

		} else if (position <= 1) { // (0,1]
			// Fade the page out.
			setAlpha(view, 1 - position);

			// Counteract the default slide transition
			setTranslationX(view, pageWidth * -position);

			// Scale the page down (between MIN_SCALE and 1)
			float scaleFactor = MIN_SCALE
					+ (1 - MIN_SCALE) * (1 - Math.abs(position));
			setScaleX(view, scaleFactor);
			setScaleY(view, scaleFactor);

		} else { // (1,+Infinity]
			// This page is way off-screen to the right.
			setAlpha(view, 0);
		}
	}

	void setAlpha(View view, float alpha) {
		ViewCompat.setAlpha(view, alpha);
	}

	void setScaleX(View view, float scaleX) {
		ViewCompat.setScaleX(view, scaleX);
	}

	void setScaleY(View view, float scaleY) {
		ViewCompat.setScaleY(view, scaleY);
	}

	void setTranslationX(View view, float translationX) {
		ViewCompat.setTranslationX(view, translationX);
	}
}