package com.develop.zuzik.navigationviewexample;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.develop.zuzik.navigationview.core.interfaces.NavigationView;
import com.develop.zuzik.navigationview.core.interfaces.NavigationViewContainer;
import com.develop.zuzik.navigationview.core.view.LeafNavigationView;

/**
 * User: zuzik
 * Date: 7/21/16
 */
public class NumberView extends LeafNavigationView {

	private final int number;
	private final TextView textView;
	private final NumberViewContainer container;

	public NumberView(final Context context, Object token, final NavigationViewContainer container, final NavigationView parentOrNull, final int number) {
		super(context, token, container, parentOrNull);
		inflate(context, R.layout.view_number, this);
		this.textView = (TextView) findViewById(R.id.number);
		this.textView.setText(String.valueOf(number));
		this.number = number;
		if (container instanceof NumberViewContainer) {
			this.container = (NumberViewContainer) container;
		} else {
			throw new RuntimeException("Need container");
		}

		findViewById(R.id.add).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				if (parentOrNull == null) {
					return;
				}

				parentOrNull
						.beginTransaction()
						.addViewToPosition(
								new NumberView(context, parentOrNull.getViews().size(), container, parentOrNull, parentOrNull.getViews().size()),
								parentOrNull.getViews().size())
						.goToViewWithToken(parentOrNull.getViews().size())
						.commit();

//				parentOrNull.addViewToEnd(new NewNumberView(context, Integer.valueOf(parentOrNull.getViews().size()), container, parentOrNull, number + 1));
			}
		});
		findViewById(R.id.remove).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				if (parentOrNull == null) {
					return;
				}
				parentOrNull
						.beginTransaction()
						.removeViewWithToken(parentOrNull.getViews().size() - 1)
						.goToViewWithToken(parentOrNull.getViews().size() - 2)
						.commit();
			}
		});
	}

	@Override
	protected void doOnStart() {
		super.doOnStart();
		this.container.showNumber(this.number);
	}
}
