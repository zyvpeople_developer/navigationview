package com.develop.zuzik.navigationviewexample.file;

import android.content.Context;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.develop.zuzik.navigationviewexample.R;

/**
 * User: zuzik
 * Date: 7/24/16
 */
public class FileView extends LinearLayout {

	private final TextView text;

	public FileView(Context context) {
		super(context);
		inflate(context, R.layout.view_file, this);
		this.text = (TextView) findViewById(R.id.text);
	}

	public void setFile(FileInfo file) {
		this.text.setText(file.name);
	}
}
