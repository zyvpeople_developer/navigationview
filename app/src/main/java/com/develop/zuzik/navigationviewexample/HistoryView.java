package com.develop.zuzik.navigationviewexample;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.develop.zuzik.navigationview.core.interfaces.NavigationView;
import com.develop.zuzik.navigationview.core.interfaces.NavigationViewContainer;
import com.develop.zuzik.navigationview.core.null_object.DebugTokenNavigationViewTitleFactory;
import com.develop.zuzik.navigationview.core.view.LeafNavigationView;
import com.develop.zuzik.navigationview.viewpager.ViewPagerContainer;
import com.develop.zuzik.navigationview.viewpager.ViewPagerNavigationViewFactory;
import com.develop.zuzik.navigationviewexample.view_pager_transformer.DepthPageTransformer;
import com.develop.zuzik.navigationview.viewpager.component.ComponentBuilder;
import com.ogaclejapan.smarttablayout.SmartTabLayout;

/**
 * User: zuzik
 * Date: 7/21/16
 */
public class HistoryView extends LeafNavigationView {

	public final NavigationView navigationView;

	public HistoryView(final Context context, Object token, final NavigationViewContainer container, NavigationView parentOrNull) {
		super(context, token, container, parentOrNull);
		inflate(context, R.layout.view_history, this);

		FrameLayout content = (FrameLayout) findViewById(R.id.content);

		navigationView = new ViewPagerNavigationViewFactory(
				context,
				new ComponentBuilder()
						.setPageTransformer(new DepthPageTransformer()))
				.create("ViewPager", container, parentOrNull);
		content.addView((View) navigationView, new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

		navigationView
				.beginTransaction()
				.addViewToPosition(createNavigationView(0), 0)
				.addViewToPosition(createNavigationView(1), 1)
				.addViewToPosition(createNavigationView(2), 2)
				.goToViewWithToken(2)
				.commit();

		findViewById(R.id.red).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				navigationView.beginTransaction().goToViewWithToken(0).commit();
			}
		});
		findViewById(R.id.green).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				navigationView.beginTransaction().goToViewWithToken(1).commit();
			}
		});
		findViewById(R.id.blue).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				navigationView.beginTransaction().goToViewWithToken(2).commit();
			}
		});

		if (navigationView instanceof ViewPagerContainer) {
			SmartTabLayout viewPagerTab = (SmartTabLayout) findViewById(R.id.viewpagertab);
			viewPagerTab.setViewPager(((ViewPagerContainer) navigationView).getViewPager());
		}
	}

	private NavigationView createNavigationView(Object token) {
		NavigationView parent = new ViewPagerNavigationViewFactory(
				getContext(),
				new ComponentBuilder()
						.doNotSaveViewsHistory()
						.denySwipe()
						.denyPageAnimation()
						.setTitleFactory(DebugTokenNavigationViewTitleFactory.INSTANCE)
						.setLayoutResId(R.layout.view_custom_view_pager_navigation_view))
				.create(token, getContainer(), navigationView);

		NavigationView child = new NumberView(getContext(), 0, getContainer(), parent, 0);
		parent.
				beginTransaction()
				.addViewToPosition(child, 0)
				.goToViewWithToken(0)
				.commit();
		return parent;
	}

	@Override
	public void doOnStart() {
		super.doOnStart();
		navigationView.onStart();
	}

	@Override
	public void doOnStop() {
		super.doOnStop();
		navigationView.onStop();
	}
}
