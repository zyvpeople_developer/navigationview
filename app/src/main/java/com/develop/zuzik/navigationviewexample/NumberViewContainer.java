package com.develop.zuzik.navigationviewexample;

/**
 * User: zuzik
 * Date: 7/21/16
 */
public interface NumberViewContainer {
	void showNumber(int number);
}
