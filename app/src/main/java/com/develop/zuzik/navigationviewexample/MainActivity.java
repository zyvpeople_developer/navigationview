package com.develop.zuzik.navigationviewexample;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.FrameLayout;

import com.develop.zuzik.filenavigationview.FileNavigationViewWrapper;
import com.develop.zuzik.navigationview.core.history_strategy.SaveNavigationViewHistoryStrategy;
import com.develop.zuzik.navigationview.core.interfaces.NavigationView;
import com.develop.zuzik.navigationview.core.interfaces.NavigationViewContainer;
import com.develop.zuzik.navigationview.core.log.LogValue;
import com.develop.zuzik.navigationview.core.log.NavigationViewLog;
import com.develop.zuzik.navigationview.list.ListNavigationViewFactory;
import com.develop.zuzik.navigationviewexample.file.FilesViewContainer;
import com.develop.zuzik.navigationviewexample.file.FilesViewNavigationViewFactory;

//TODO: in VPNavigationView when remove view try firstly set current and then notify adapter (check if current exists)
//TODO: when remove item we have warning - go to current again - try to check if current is not started
public class MainActivity extends AppCompatActivity
		implements NumberViewContainer, NavigationViewContainer, FilesViewContainer {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		FrameLayout content = (FrameLayout) findViewById(R.id.content);

		fileViewExample(content);
//		oldExample(content);
	}

	FileNavigationViewWrapper wrapper;

	private void fileViewExample(FrameLayout content) {
//		final NavigationView navigationView = new ViewPagerNavigationViewFactory(
//				this,
//				new ComponentBuilder()
//						.setLayoutResId(R.layout.view_custom_view_pager_navigation_view)
//						.setTitleFactory(new NavigationViewTitleFactory() {
//							@Override
//							public String create(Object token) {
//								return token.toString();
//							}
//						}))
//				.create("root token", this, null);
		final NavigationView navigationView = new ListNavigationViewFactory(
				this,
				SaveNavigationViewHistoryStrategy.INSTANCE)
				.create("root token", this, null);
		content.addView((View) navigationView);
		wrapper = new FileNavigationViewWrapper(navigationView, new FilesViewNavigationViewFactory(this));
		wrapper.navigateToFile("/");

		findViewById(R.id.start).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				navigationView.onStart();
			}
		});
		findViewById(R.id.stop).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				navigationView.onStop();
			}
		});
		findViewById(R.id.logViews).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				NavigationViewLog.logTree(MainActivity.this, navigationView, LogValue.TOKEN, LogValue.IS_STARTED);
			}
		});
		findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				wrapper.navigateBack();
			}
		});
	}

	@Override
	public void onFileClicked(String absolutePath) {
		wrapper.navigateToFile(absolutePath);
	}

	@Override
	public void onFileRemoved(String absolutePath) {
		wrapper.removeFile(absolutePath);
	}

	private void oldExample(FrameLayout content) {
		final HistoryView historyView = new HistoryView(this, "HistoryView", this, null);
		content.addView(historyView);

		findViewById(R.id.start).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				historyView.onStart();
			}
		});
		findViewById(R.id.stop).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				historyView.onStop();
			}
		});
		findViewById(R.id.logViews).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				NavigationViewLog.logTree(MainActivity.this, historyView);
				NavigationViewLog.logTree(MainActivity.this, historyView.navigationView, LogValue.TOKEN, LogValue.IS_STARTED);
			}
		});
	}

	//region NumberViewContainer

	@Override
	public void showNumber(int number) {
//		Log.i("MainActivity", "NumberViewContainer " + String.valueOf(number));
	}

	//endregion

}
