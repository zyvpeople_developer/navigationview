package com.develop.zuzik.navigationviewexample.file;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * User: zuzik
 * Date: 7/25/16
 */
public class FilesRepository {

	public static final FilesRepository INSTANCE = new FilesRepository();

	private final FileInfo root;

	private FilesRepository() {
		FileInfo songA = new FileInfo("SongA", "/Music/SongA", Collections.EMPTY_LIST);
		FileInfo songB = new FileInfo("SongB", "/Music/SongB", Collections.EMPTY_LIST);
		FileInfo songC = new FileInfo("SongC", "/Music/SongC", Collections.EMPTY_LIST);
		FileInfo music = new FileInfo("Music", "/Music", Arrays.asList(songA, songB, songC));

		FileInfo imageA = new FileInfo("ImageA", "/Images/ImageA", Collections.EMPTY_LIST);
		FileInfo imageB = new FileInfo("ImageB", "/Images/ImageB", Collections.EMPTY_LIST);
		FileInfo imageC = new FileInfo("ImageC", "/Images/ImageC", Collections.EMPTY_LIST);
		FileInfo images = new FileInfo("Images", "/Images", Arrays.asList(imageA, imageB, imageC));

		FileInfo documentA = new FileInfo("DocumentA", "/Documents/DocumentA", Collections.EMPTY_LIST);
		FileInfo documentB = new FileInfo("DocumentB", "/Documents/DocumentB", Collections.EMPTY_LIST);
		FileInfo documentC = new FileInfo("DocumentC", "/Documents/DocumentC", Collections.EMPTY_LIST);

		FileInfo privateDocumentA = new FileInfo("PrivateDocumentA", "/Documents/PrivateDocuments/PrivateDocumentA", Collections.EMPTY_LIST);
		FileInfo privateDocumentB = new FileInfo("PrivateDocumentB", "/Documents/PrivateDocuments/PrivateDocumentB", Collections.EMPTY_LIST);
		FileInfo privateDocumentC = new FileInfo("PrivateDocumentC", "/Documents/PrivateDocuments/PrivateDocumentC", Collections.EMPTY_LIST);
		FileInfo privateDocuments = new FileInfo("PrivateDocuments", "/Documents/PrivateDocuments", Arrays.asList(privateDocumentA, privateDocumentB, privateDocumentC));

		FileInfo documents = new FileInfo("Documents", "/Documents", Arrays.asList(documentA, documentB, documentC, privateDocuments));

		root = new FileInfo("/", "/", Arrays.asList(music, images, documents));
	}

	public List<FileInfo> getFilesForPath(String path) {
		return getFilesForPath(root, path);
	}

	public List<FileInfo> getFilesForPath(FileInfo directory, String path) {
		if (directory.absolutePath.equals(path)) {
			return directory.children;
		} else {
			for (FileInfo file : directory.children) {
				List<FileInfo> files = getFilesForPath(file, path);
				if (!files.isEmpty()) {
					return files;
				}
			}
		}
		return Collections.EMPTY_LIST;
	}
}
