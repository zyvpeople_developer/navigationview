package com.develop.zuzik.navigationviewexample.file;

import android.content.Context;

import com.develop.zuzik.filenavigationview.FileNavigationViewFactory;
import com.develop.zuzik.navigationview.core.interfaces.NavigationView;
import com.develop.zuzik.navigationview.core.interfaces.NavigationViewContainer;

/**
 * User: zuzik
 * Date: 7/25/16
 */
public class FilesViewNavigationViewFactory implements FileNavigationViewFactory {

	private final Context context;

	public FilesViewNavigationViewFactory(Context context) {
		this.context = context;
	}

	@Override
	public NavigationView create(String path, Object token, NavigationViewContainer container, NavigationView parent) {
		return new FilesView(this.context, path, token, container, parent);
	}
}
