package com.develop.zuzik.navigationviewexample.file;

/**
 * User: zuzik
 * Date: 7/25/16
 */
public interface FilesViewContainer {
	void onFileClicked(String absolutePath);

	void onFileRemoved(String absolutePath);
}
