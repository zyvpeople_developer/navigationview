package com.develop.zuzik.navigationviewexample.file;

import java.util.ArrayList;
import java.util.List;

/**
 * User: zuzik
 * Date: 7/25/16
 */
public class FileInfo {

	public String name = "";
	public String absolutePath = "";
	public List<FileInfo> children = new ArrayList<>();

	public FileInfo(String name, String absolutePath, List<FileInfo> children) {
		this.name = name;
		this.absolutePath = absolutePath;
		this.children = children;
	}
}
