package com.develop.zuzik.navigationviewexample.file;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.develop.zuzik.navigationview.core.interfaces.NavigationView;
import com.develop.zuzik.navigationview.core.interfaces.NavigationViewContainer;
import com.develop.zuzik.navigationview.core.view.LeafNavigationView;
import com.develop.zuzik.navigationviewexample.R;

import java.util.List;

/**
 * User: zuzik
 * Date: 7/24/16
 */
public class FilesView extends LeafNavigationView {

	private RecyclerView recyclerView;
	private RecyclerView.Adapter adapter;
	private FilesViewContainer container;

	public FilesView(Context context, final String path, Object token, final NavigationViewContainer container, NavigationView parent) {
		super(context, token, container, parent);
		inflate(context, R.layout.view_files, this);

		if (getContainer() instanceof FilesViewContainer) {
			this.container = (FilesViewContainer) getContainer();
		} else {
			throw new RuntimeException("Container must implement " + FilesViewContainer.class.getSimpleName());
		}

		this.recyclerView = (RecyclerView) findViewById(R.id.recyclerView);

		this.recyclerView.setHasFixedSize(true);

		this.recyclerView.setLayoutManager(new LinearLayoutManager(context));

		this.adapter = new FilesViewAdapter(FilesRepository.INSTANCE.getFilesForPath(path));
		this.recyclerView.setAdapter(this.adapter);

		((SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout))
				.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
					@Override
					public void onRefresh() {
						FilesView.this.container.onFileRemoved(path);
					}
				});
	}

	public class FileViewHolder extends RecyclerView.ViewHolder {

		FileView fileView;

		public FileViewHolder(FileView view) {
			super(view);
			this.fileView = view;
		}
	}

	public class FilesViewAdapter extends RecyclerView.Adapter<FileViewHolder> {

		final List<FileInfo> files;

		public FilesViewAdapter(List<FileInfo> files) {
			this.files = files;
		}

		@Override
		public FileViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
			return new FileViewHolder(new FileView(parent.getContext()));
		}

		@Override
		public void onBindViewHolder(FileViewHolder holder, int position) {
			final FileInfo file = this.files.get(position);
			holder.fileView.setFile(file);
			holder.fileView.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View view) {
					container.onFileClicked(file.absolutePath);
				}
			});
		}

		@Override
		public int getItemCount() {
			return this.files.size();
		}
	}

}
